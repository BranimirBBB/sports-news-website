# Sports news website

This project was a one-week project in Semester 4 (Smart mobile specialization) at Fontys University of applied science in Eindhoven, The Netherlands.

I was working in a duo with M.Tomov and our challenge was to create a prototype of sports application for a given persona.

We decided to create a Figma prototype of a Sports news application which we called "Sportal".

More about the project you can find in "Sportal documentation".
